const { verifyToken, addOrUpdateUser, getCurrentUser } = require("suretrack-backend");

class AuthorizeMiddleware {
  static token(req, res, next) {
    const token = req.header("Authorization")?.split(" ")[1];
    const ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;

    if (!token) return res.status(401).send({
      message: 'Authorization required attach properly.',
      responseCode: 401
    });
    const userData = verifyToken(token);
    if (!userData) {
      return res.status(401).send({
        message: "Token is not valid",
        responseCode: 401,
      });
    } else {
      req.user = userData;
      addOrUpdateUser(userData.id_dd_user, {
        ...userData,
        ipAddress: ip,
        last_login: new Date(),
      });
      next();
    }
  }

  static roles(roles) {
    /**
     * Data Kewenangan (Roles) dd_user_group
     * @param {Array} roles - Array data role yang didapatkan dari route
     * 1 = Super Administrator 50 = Operasional Cabang
     */
    return (req, res, next) => {
      // console.log("User Data", req.user);
      const session = getCurrentUser(req.user.id_dd_user);

      if (!roles.includes(session.id_dd_user_group)) {
        return res.send({
            message: `Akses user ${req.user.nama_group} ditolak`
        });
      }
      next();
    };
  }
}


module.exports = AuthorizeMiddleware;
