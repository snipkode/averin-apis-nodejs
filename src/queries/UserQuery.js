const { hashMD5 } = require("suretrack-backend");

class UserQuery {
  static login(payload) {
    const { username, password } = payload;
    const sql = `
            SELECT 
                a.username,
                a.id_dd_group,
                a.id_dd_user_group,
                a.id_dd_user,
                b.nama_group ,
                b.lompatan,
                b.keterangan
            FROM dd_user a
                LEFT JOIN dd_user_group b ON b.id_dd_user_group = a.id_dd_user_group
            WHERE 
                a.username = '${username}' 
            AND a.password = '${hashMD5(password)}'
        `;

    return sql;
  }

  static selectAll(payload) {
    const { limit, offset, search, orderBy } = payload;
    const filter = search ? `WHERE username LIKE '%${search}%'` : "";
    const filter2 = orderBy ? `ORDER BY id_dd_user ${orderBy}` : "";
    const sql = `
            SELECT 
                a.username,
                a.password,
                a.id_dd_group,
                a.id_dd_user_group,
                a.ko_wil AS id_dc_wilayah_kerja,
                a.id_dd_user,
                b.nama_group ,
                b.lompatan,
                b.keterangan,
                a.npp,
                a.input_tgl,
                a.id_in_branch
            FROM dd_user a
                LEFT JOIN dd_user_group b ON b.id_dd_user_group = a.id_dd_user_group
            ${filter}
            ${filter2}
            LIMIT ${limit} OFFSET ${offset} 
        `;

    return sql;
  }

  static updateUser(payload) {
    const { status, password, npp, id_dd_user } = payload;
    const updateNpp = npp ? `,npp = '${npp}'` : "";

    const sql = `
                    UPDATE dd_user 
                    SET 
                        password = '${hashMD5(password)}',
                        status = ${status}
                        ${updateNpp}
                    WHERE 
                        id_dd_user = ${id_dd_user}
                `;
    return sql;
  }

  static insertUser(payload) {
    const { username, password, status, input_tgl, input_id, id_dd_user_group, ko_wil, npp } =
      payload;
    let sql = `
        INSERT INTO 
            dd_user (username, password, status`;

    if (input_tgl !== undefined) {
      sql += ", input_tgl";
    }
    if (input_id !== undefined) {
      sql += ", input_id";
    }

    sql += ", id_dd_user_group, ko_wil, npp) VALUES (";
    sql += `'${username}', '${hashMD5(password)}', '${status}'`;
    
    if (input_tgl !== undefined) {
      sql += `, '${input_tgl}'`;
    }

    if (input_id !== undefined) {
      sql += `, ${input_id}`;
    }

    sql += `, '${id_dd_user_group}', '${ko_wil}', '${npp}')`;

    return sql;
  }

  static getUserByUsername(username) {
    const sql = `
            SELECT 
                a.username,
                a.id_dd_user,
                a.id_dd_user_group,
                a.npp
            FROM dd_user a
                LEFT JOIN dd_user_group b ON b.id_dd_user_group = a.id_dd_user_group
            WHERE 
                a.username = '${username}'
        `;

    return sql;
  }
}

module.exports = UserQuery;
