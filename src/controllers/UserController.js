const { Joi, db, logger, addOrUpdateUser, createToken } = require("suretrack-backend");
const UserQuery                                         = require("../queries/UserQuery");

class UserController {
    static getFormattedDateTime() {
        // Buat objek Date untuk tanggal dan waktu saat ini
        const sekarang = new Date();
    
        // Dapatkan tahun, bulan, dan hari
        const tahun = sekarang.getFullYear(); // TTTT
        const bulan = String(sekarang.getMonth() + 1).padStart(2, '0'); // BB (bulan dimulai dari 0, jadi tambahkan 1)
        const hari = String(sekarang.getDate()).padStart(2, '0'); // HH
    
        // Dapatkan jam, menit, dan detik
        const jam = String(sekarang.getHours()).padStart(2, '0'); // JJ
        const menit = String(sekarang.getMinutes()).padStart(2, '0'); // MM
        const detik = String(sekarang.getSeconds()).padStart(2, '0'); // DD
    
        // Buat format TTTT-BB-HH dan JJ:MM:DD
        const formatTgl = `${tahun}-${bulan}-${hari}`; // TTTT-BB-HH
        const formatWkt = `${jam}:${menit}:${detik}`; // JJ:MM:DD
    
        // Kembalikan format tanggal dan waktu
        return `${formatTgl} ${formatWkt}`;
    }
    
    static async login(req, res){
        try {
            const { error } = Joi.object({
                username: Joi.string().required().messages({
                    "string.base": "Parameter username wajib string",
                    "any.required": "Paramater username wajib diisi"
                }),
                password: Joi.string().required().messages({
                    "string.base": "Parameter password wajib string",
                    "any.required": "Paramater password wajib diisi"
                }),
            }).validate(req.body);

            if (error) {
                logger.error("ERROR LOGIN VALIDATION : ", error);
                return res.status(500).send({
                    message: error.details[0].message,
                    responseCode: 500
                });
            };

            const query  = UserQuery.login(req.body);
            const result = await db.executePg(query);

            if (result.rowCount > 0) {

                const dt          = result.rows[0];
                const dataToken   = { 
                    id_dd_user: dt.id_dd_user, 
                    id_dd_user_group: dt.id_dd_user_group, 
                    username: dt.username, 
                    nama_group: dt.nama_group,
                    socket_id: null, // inisialisasi pengguna online yang terhubung dengan socket
                    last_login: new Date()
                }

                addOrUpdateUser(dt.id_dd_user, dataToken);

                return res.status(200).send({
                    message: "Login berhasil",
                    responseCode: 200,
                    accessToken: createToken(dataToken, { expiresIn: '30m' }),
                    currentUser: dataToken
                });

            } else {
                return res.status(401).send({
                    message: "Login ditolak",
                    responseCode: 401
                })
            }

        } catch (error) {
            logger.error("Error:", error);
            return res.status(500).send({
                message: "Terjadi kesalahan saat login",
                responseCode: 500
            });
        }
    }

    static async getUser(req, res){
        try {
            const { error } = Joi.object({
                limit: Joi.number().required().messages({
                    "number.base": "Parameter limit wajib angka",
                    "any.required": "Paramater limit wajib diisi"
                }),
                offset: Joi.number().required().messages({
                    "number.base": "Parameter offset angka",
                    "any.required": "Paramater offset wajib diisi"
                }),
                search: Joi.string().optional().messages({
                    "string.base" : "Paramater search wajib string"
                }),
                orderBy: Joi.string().optional().messages({
                    "string.base" : "Paramater orderBy wajib disi desc atau asc"
                })
            }).validate(req.query);

            if (error) {
                logger.error("ERROR VALIDATION : ", error);
                return res.status(500).send({
                    message: error.details[0].message,
                    responseCode: 500
                });
            };

            const query  = UserQuery.selectAll(req.query);
            const result = await db.executePg(query);

            return res.json({
                message: "berhasil",
                responseCode: 200,
                data: {
                    total: result.rowCount,
                    data: result.rows
                }
            });

        } catch (error) {
            logger.error("Error:", error);
            return res.status(500).send({
                message: "Terjadi kesalahan",
                responseCode: 500
            });
        }
    }

    static async updateUser(req, res){
        try {
            /**
             * Fungsi Validasi Payload Update User
             * @RequestBody - {password, npp, status}
             * @return {Object} - ERROR
             */
            const schema = Joi.object({
                password: Joi.string().required().messages({
                    "string.base": "Request body password wajib string",
                    "any.required": "Request body password wajib diisi"
                }),
                status: Joi.number().required().messages({
                    "number.base": "Request body status wajib angka",
                    "any.required": "Request body status wajib diisi"
                }),
                npp: Joi.string().optional().messages({
                    "string.base": "Request body npp wajib angka",
                    "any.required": "Request body npp wajib diisi"
                }),
                id_dd_user: Joi.number().optional().messages({
                    "number.base": "Request body id_dd_user wajib angka",
                    "any.required": "Request body id_dd_user wajib diisi"
                }),
            });

            const {error} = schema.validate(req.body);

            if(error){
                logger.error("Error:", error);
                return res.status(500).send({
                    message: error.details[0].message,
                    responseCode: 500
                })
            }
            const query  = UserQuery.updateUser(req.body);
            const result = await db.executePg(query);

            if(result.rowCount == 1){
                return res.json({
                    message: `Update data user dengan id_dd_user ${req.body.id_dd_user}`,
                    responseCode: 200
                })
            } else {
                return res.status(404).send({
                    message: `Update data user dengan id_dd_user ${req.body.id_dd_user} tidak ditemukan.`,
                    responseCode: 404
                })
            }
            
        } catch (error) {
            logger.error("Error:", error);
            return res.status(500).send({
                message: "Terjadi kesalahan saat update user",
                responseCode: 500
            });
        }
    }

    static async insertUser(req, res){
        const dateTime = UserController.getFormattedDateTime();
        try {
            /**
             * Validasi Request Body
             * @RequestBody - Data {username, password, status, input_tgl, id_dd_user_group, ko_wil}
             */
            const { error } = Joi.object({
                username: Joi.string().required().messages({
                    "string.base": "Parameter username wajib diisi string",
                    "any.required": "Paramater username wajib diisi"
                }),
                password: Joi.string().required().messages({
                    "string.base": "Parameter password wajib diisi string",
                    "any.required": "Paramater password wajib diisi"
                }),
                status: Joi.number().required().messages({
                    "number.base": "Parameter status wajib diisi number",
                    "any.required": "Paramater status wajib diisi"
                }),
                id_dd_user_group: Joi.number().required().messages({
                    "number.base": "Parameter id_dd_user_group wajib diisi number",
                    "any.required": "Paramater id_dd_user_group wajib diisi"
                }),
                ko_wil: Joi.number().required().messages({
                    "number.base": "Parameter ko_wil wajib diisi number",
                    "any.required": "Paramater ko_wil wajib diisi"
                }),
                npp: Joi.string().required().messages({
                    "string.base": "Parameter npp wajib diisi number",
                    "any.required": "Paramater npp wajib diisi"
                }),
                input_tgl: Joi.date().optional().messages({
                    "date.base": "Parameter input_tgl wajib diisi date type",
                    "any.required": "Paramater input_tgl wajib diisi"
                }),
                input_id: Joi.number().optional().messages({
                    "date.base": "Parameter input_id wajib diisi number",
                    "any.required": "Paramater input_id wajib diisi"
                }),
            }).validate(req.body);

            if(error){
                return res.status(500).send({
                    message: error.details[0].message,
                    responseCode: 500
                });
            }

            const selectUser   = UserQuery.getUserByUsername(req.body.username);
            const getUserExist = await db.executePg(selectUser);
            const userExist    = getUserExist.rows[0];
            

            if(getUserExist.rowCount > 0){

                if(userExist.npp == req.body.npp){
                    return res.status(401).send({
                        message: "NPP telah digunakan oleh user lain.",
                        responseCode: 401
                    })
                }
                if(userExist.username == req.body.username){
                    return res.status(401).send({
                        message: "Username telah digunakan oleh user lain.",
                        responseCode: 401
                    })
                }
                
            }

            const query  = UserQuery.insertUser({...req.body, input_id: req.user.id_dd_user, input_tgl: dateTime});
            const result = await db.executePg(query);

            if(result.rowCount){
                return res.send({
                    message: "User berhasil ditambahkan.",
                    responseCode: 200
                })
            }

        } catch (error) {
            logger.error('ERROR : ', error)
            return res.status(500).send({
                message: "Internal server error",
                responseCode: 500
            })
        }
    }

}

module.exports = UserController;