const { router }          = require('suretrack-backend');
const API                 = router();
const UserController      = require('../controllers/UserController');
const AuthorizeMiddleware = require('../middlewares/AuthorizeMiddleware');

API.post('/users', AuthorizeMiddleware.token, AuthorizeMiddleware.roles([1]), UserController.getUser);
API.put('/users', AuthorizeMiddleware.token, AuthorizeMiddleware.roles([1]), UserController.updateUser);
API.post('/users/add', AuthorizeMiddleware.token, AuthorizeMiddleware.roles([1]), UserController.insertUser);
API.post('/users/login', UserController.login);

module.exports = API;