const { router }         = require('suretrack-backend');
const WEB                = router();

WEB.get('/', (req, res) => {
    return res.send("Public Route")
});

WEB.get('/admin', (req, res) => {
    return res.send("Admin Route")
});
WEB.get('/cabang', (req, res) => {
    return res.send("Cabang Route")
});

module.exports = WEB;