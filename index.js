const { app, json, cors, db } = require('suretrack-backend');
const apis                    = require('./src/routes/api');
const webs                    = require('./src/routes/web');

app.use(json());
app.use(cors());

(async () => {
    await db.connectToMssql();
    await db.connectToPg();
})();

/**
 * Route Path WEB
 * @function router - Router API Expresss
 * @returns  {req, res}
 */
app.use("/", webs);

/**
 * Route Path API
 * @function router - Router API Expresss
 * @returns  {req, res}
 */
app.use("/api", apis);
